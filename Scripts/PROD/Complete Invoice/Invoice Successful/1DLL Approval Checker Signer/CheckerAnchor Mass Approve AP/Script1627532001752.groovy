import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

public class checkAll {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class createBatchButton {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class createBatchNote {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class submitBatch {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class password {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class refreshButton {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class scroll {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.setText(log.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userCheckerAnchor)
//password
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)

WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)
//button sumbit
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 /////////////////
 WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
 //inbox invoice AP
 WebUI.click(log.getTestObjectWithXpath('//*[@id="0304"]'))

WebUI.click(checkAll.getTestObjectWithXpath('//*[@id="checkboxMultiple"]')) //ceklis semua invoice

WebUI.click(createBatchButton.getTestObjectWithXpath('//*[@id="ui-id-2"]/form/div[2]/button')) //create batch

WebUI.setText(createBatchNote.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/div[2]/div[2]/div/textarea'), 'Katalon')

WebUI.click(createBatchButton.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/div[3]/button[2]'))// create

WebUI.verifyTextPresent('Insert Batch Data Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(submitBatch.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[4]/div/div/div/button[2]'))

WebUI.setText(createBatchNote.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div/div[2]/div/textarea'), 'Katalon')

WebUI.click(submitBatch.getTestObjectWithXpath('//*[@id="submitapprove"]'))

WebUI.verifyTextPresent('Success. Verification On Progress', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(20)

WebUI.click(refreshButton.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/div[2]/button'))

WebUI.takeFullPageScreenshot()

 WebUI.click(scroll.getTestObjectWithXpath('//*[@id="DataTables_Table_0_wrapper"]/div[3]/div[2]'))
 
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(scroll.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 
 WebUI.takeFullPageScreenshot()
 
// WebUI.verifyElementText(scroll.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[1]/td[8]'), 'VALID')
// WebUI.verifyElementText(scroll.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[2]/td[8]'), 'VALID')
 


WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

