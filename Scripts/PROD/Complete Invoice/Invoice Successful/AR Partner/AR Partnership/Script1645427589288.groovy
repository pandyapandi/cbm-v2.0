import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)
  
WebUI.callTestCase(findTestCase('PROD/Complete Invoice/Invoice Successful/AR Partner/AR Partnership Maker'),
	[:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('PROD/Complete Invoice/Invoice Successful/1DLL Approval Checker Signer/CheckerAnchor Approve SCFAP'),
	[:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)
 
WebUI.callTestCase(findTestCase('PROD/Complete Invoice/Invoice Successful/1DLL Approval Checker Signer/SignerAnchor Approve SCFAP'),
	[:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.closeBrowser()

