import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)



///////Maker
public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="0201"]')) //New Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="hours-available-progress"]/h3')) //Single Entry

WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-entitas_partner-container"]')) // select Company
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.partner3)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-payment_method-container"]'))
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.paymentSCFAR)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="amount"]'), GlobalVariable.amount)

Date today = new Date()
String currentDate = today.format('dd-MM-yyyy')
String invDate = today.format('ddMMyyyy')
String invTime = today.format('hhmmss')
settle = (today + 1)
sharing = (today + 1)
String settlementDate = settle.format('dd-MM-yyyy')
String sharinglimitDate = settle.format('dd-MM-yyyy')
//String InvNo = (currentDate + '-') + currentTime
String noInv = (('StagCBM-AR' + invDate) + '-') + invTime
println(noInv)
println(currentDate)
println(settlementDate)

WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), currentDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="maturity_date"]'), settlementDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="entitas_inv_no"]'), noInv)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="form_add"]/div[1]/div[11]/div/input'), noInv)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submit-user"]/button'))
WebUI.delay(20)
WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))


//////Checker




//Checker approval
//username
WebUI.setText(log.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userCheckerAnchor)
//password
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

//button sumbit
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
/////////////////
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a'))
//inbox invoice AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0204"]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_checker"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_checker"]/div/div/form/div[2]/button[2]'))
//Verifikasi sukses
WebUI.verifyTextPresent('success', false, FailureHandling.STOP_ON_FAILURE)
//Log out
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//////Signer



//username
WebUI.setText(log.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userSignerAnchor)
//password
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

//button sumbit
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
/////////////////
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a'))
//inbox invoice AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0205"]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_signer"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.setText(log.getTestObjectWithXpath('//*[@id="otp_code"]'), GlobalVariable.otpCode)
 
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_signer"]/div/div/form/div[2]/button[2]'))


if(WebUI.verifyTextPresent('successful', true, FailureHandling.CONTINUE_ON_FAILURE)){
	WebUI.delay(1)
	WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
	 WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
	 WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	println("---------success-------------------")
	
	}else {
	WebUI.takeScreenshot()
	WebUI.click(reject.getTestObjectWithXpath('//*[@id="reject"]'))
	WebUI.setText(reject.getTestObjectWithXpath('//*[@id="modal_reject_signer"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(reject.getTestObjectWithXpath('//*[@id="modal_reject_signer"]/div/div/form/div[2]/button[2]'))
	println("---------invoice rejected")
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	}



///////////////////////////////Aproval maker partner

public class login {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class invoiceAP {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class details {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class btn {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class logout {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class note {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

//username
WebUI.setText(login.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerPartner3)
//password
WebUI.setText(login.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

//button sumbit
WebUI.click(login.getTestObjectWithXpath('//*[@id="submitsignin"]'))

/////////////////
WebUI.click(invoiceAP.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
//inbox invoice AP
WebUI.click(invoiceAP.getTestObjectWithXpath('//*[@id="0306"]'))
WebUI.click(details.getTestObjectWithXpath('//*[@id="row_0"]/td[9]/div/a'))
WebUI.click(btn.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div/button[2]'))
WebUI.setText(note.getTestObjectWithXpath('//*[@id="modal_approve_maker_confirm"]/div/div/form/div[1]/div[4]/div/textarea'), 'OK')
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_approve_maker_confirm"]/div/div/form/div[2]/button[2]'))
//Verifikasi sukses
WebUI.verifyTextPresent('success', false, FailureHandling.CONTINUE_ON_FAILURE)

//Log out
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//Close Notification
 
 

//Checker approval
//username
WebUI.setText(login.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userCheckerPartner3)
//password
WebUI.setText(login.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

//button sumbit
WebUI.click(login.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 
/////////////////
WebUI.click(invoiceAP.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
//inbox invoice AP
WebUI.click(invoiceAP.getTestObjectWithXpath('//*[@id="0304"]'))
WebUI.click(details.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(note.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/form/div[2]/button[2]'))
//Verifikasi sukses
WebUI.verifyTextPresent('success', false, FailureHandling.CONTINUE_ON_FAILURE)

//Log out
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//Close Notification
 

//Signer approval
//username
WebUI.setText(login.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userSignerPartner3)
//password
WebUI.setText(login.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 

//button sumbit
WebUI.click(login.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 
/////////////////
WebUI.click(invoiceAP.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
//inbox invoice AP
WebUI.click(invoiceAP.getTestObjectWithXpath('//*[@id="0305"]'))
WebUI.click(details.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(note.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.setText(log.getTestObjectWithXpath('//*[@id="otp_code"]'), GlobalVariable.otpCode)
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[2]/button[2]'))
WebUI.delay(20)
if(WebUI.verifyTextPresent('success', true, FailureHandling.CONTINUE_ON_FAILURE)){
	
	WebUI.delay(1)
    WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
    WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
    WebUI.click(logout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
    WebUI.closeBrowser()
	
	}else {
	WebUI.takeScreenshot(1)
	WebUI.click(login.getTestObjectWithXpath('//*[@id="reject"]'))
	WebUI.setText(login.getTestObjectWithXpath('//*[@id="modal_reject_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(login.getTestObjectWithXpath('//*[@id="reject_signer"]'))
	println("---------invoice rejected")
	WebUI.delay(2)
	WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
    WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
    WebUI.click(logout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
    WebUI.closeBrowser()

	}



 



