import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase


public class logout {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}


WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.delay(1)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="0201"]')) //New Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="hours-available-progress"]/h3')) //Single Entry

WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-entitas_partner-container"]')) // select Company
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.partner2)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-payment_method-container"]'))
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.paymentDF)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="amount"]'), GlobalVariable.amount)

Date today = new Date()
currendate1 = (today + 2)
String currentDate = currendate1.format('dd-MM-yyyy')
String invDate = today.format('ddMMyyyy')
String invTime = today.format('hhmmss')
settle = (today + 2)
sharing = (today + 1)
String settlementDate = settle.format('dd-MM-yyyy')
String sharinglimitDate = settle.format('dd-MM-yyyy')
//String InvNo = (currentDate + '-') + currentTime
String noInv = (('DF-insta' + invDate) + '-') + invTime
println(noInv)
println(currentDate)
println(settlementDate)

WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), settlementDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="entitas_inv_no"]'), noInv)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="form_add"]/div[1]/div[11]/div/input'), noInv)

WebUI.uploadFile(log.getTestObjectWithXpath('//*[@id="invoice_file"]'), 'C:\\FileUploadCBM.pdf', FailureHandling.OPTIONAL)

WebUI.click(log.getTestObjectWithXpath('//*[@id="submit-user"]/button'))
WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshot()

WebUI.delay(2)


//Log out
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(logout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('PROD/Complete Invoice/Invoice Successful/1DLL Approval Checker Signer/CheckerAnchor Approve AR'), 
    [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('PROD/Complete Invoice/Invoice Successful/1DLL Approval Checker Signer/SignerAnchor Approve AR'),
	[:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)


////////////////////////////////////////// Approve instapay

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.delay(1)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="0203"]')) //New Invoice AR
WebUI.setText(log.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), noInv) //search inv
WebUI.click(log.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[8]/div/a')) //details
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[3]/div[2]/div/div[2]/div/button')) //button instapay
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_instapay_maker"]/div/div/form/div[1]/div[2]/div/textarea'), noInv) //note instapay
WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), currentDate)
WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="payment_date"]'), Keys.chord(
        Keys.ENTER))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), currentDate)
 WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="payment_date"]'), Keys.chord(
		 Keys.ENTER))
 WebUI.delay(1)
 WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="payment_date"]'), Keys.chord(
	  Keys.ENTER))
 
 WebUI.delay(3)

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshot()

WebUI.delay(2)

public class logOut {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
public class reject {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
 
////////////////////////////////////////////

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userCheckerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="0204"]')) //invoice checker
WebUI.click(log.getTestObjectWithXpath('//*[@id="ui-id-7"]')) //instapay
WebUI.click(log.getTestObjectWithXpath('//*[@id="ui-id-8"]/div/div/div/div/div/table/tbody/tr/td[8]/div/a')) //details instapay

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div[2]/button[2]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_checker_instapay"]/div/div/form/div[1]/div/div/textarea'), 'ok')
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_checker_instapay"]/div/div/form/div[2]/button[2]')) //approve

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)
WebUI.takeFullPageScreenshot()

WebUI.delay(2)

WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.navigateToUrl(GlobalVariable.url)

//WebUI.sendKeys(log.getTestObjectWithXpath('//*[@id="modal_information"]'), Keys.chord(Keys.ESCAPE), FailureHandling.OPTIONAL)

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userSignerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="0205"]')) //invoice signer
WebUI.click(log.getTestObjectWithXpath('//*[@id="ui-id-7"]')) //instapay
WebUI.click(log.getTestObjectWithXpath('//*[@id="ui-id-8"]/div/div/div/div/div/table/tbody/tr/td[8]/div/a')) //details instapay

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div[2]/button[2]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_checker_instapay"]/div/div/form/div[1]/div[1]/div/textarea'), 'ok')
WebUI.setText(log.getTestObjectWithXpath('//*[@id="otp_code"]'), GlobalVariable.otpCode)
 
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitapprove"]')) //approve
WebUI.takeFullPageScreenshot()
if(WebUI.verifyTextPresent('success', true, FailureHandling.CONTINUE_ON_FAILURE)){
	WebUI.delay(1)
	WebUI.delay(2)
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

	println("---------success-------------------")
	
	}else {
	WebUI.takeScreenshot()
	WebUI.click(reject.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div[2]/button[1]'))
	WebUI.setText(reject.getTestObjectWithXpath('//*[@id="modal_reject_checker_instapay"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(reject.getTestObjectWithXpath('//*[@id="modal_reject_checker_instapay"]/div/div/form/div[2]/button[2]'))
	println("---------invoice rejected")
	WebUI.delay(2)
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
	WebUI.click(logOut.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	
	}

//////////////////////////////////////////
WebUI.closeBrowser()

