import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.delay(1)
WebUI.callTestCase(findTestCase('Chaptcha'), [:]	)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="0201"]')) //New Invoice AR
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div/a/div')) //bulk Entry


WebUI.uploadFile(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[1]/div/fieldset/div[2]/div/div/input'), 'C:\\Users\\rifal\\Downloads\\KatalonFile\\FileUploadAR.xlsx', FailureHandling.OPTIONAL)
WebUI.uploadFile(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[1]/div/fieldset/div[3]/div/div/input'), 'C:\\Users\\rifal\\Downloads\\KatalonFile\\FileUploadCBM.pdf', FailureHandling.OPTIONAL)

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[2]/div/button')) //submit bulk

WebUI.delay(20)

WebUI.click(log.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[6]/a')) //approve verify

WebUI.click(log.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[6]/a'), FailureHandling.OPTIONAL) //details

WebUI.takeFullPageScreenshot()

WebUI.click(log.getTestObjectWithXpath('//*[@id="DataTables_Table_0_wrapper"]/div[2]/div/div[2]'))
 
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 WebUI.sendKeys(log.getTestObjectWithXpath('/html'), Keys.chord(Keys.ARROW_RIGHT))
 
WebUI.takeFullPageScreenshot()

WebUI.click(log.getTestObjectWithXpath('//*[@id="submit_invoice"]')) //submit bulk

WebUI.acceptAlert() 

//Log out
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))