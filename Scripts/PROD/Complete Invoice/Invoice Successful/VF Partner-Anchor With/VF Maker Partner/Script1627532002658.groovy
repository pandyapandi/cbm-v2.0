import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerPartner)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[3]/a')) //Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="0201"]')) //New Invoice AR
WebUI.click(log.getTestObjectWithXpath('//*[@id="hours-available-progress"]/h3')) //Single Entry
WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-entitas_partner-container"]')) // select Company
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.anchor2)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-payment_method-container"]'))
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.paymentVF)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="amount"]'), GlobalVariable.amount)

Date today = new Date()
String currentDate = today.format('dd-MM-yyyy')
String invDate = today.format('ddMMyyyy')
String invTime = today.format('hhmmss')
settle = (today + 1)
sharing = (today + 1)
String settlementDate = settle.format('dd-MM-yyyy')
String sharinglimitDate = settle.format('dd-MM-yyyy')
//String InvNo = (currentDate + '-') + currentTime
String noInv = (('StagCBM-VF' + invDate) + '-') + invTime
println(noInv)
println(currentDate)
println(settlementDate)

WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), currentDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="maturity_date"]'), settlementDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="sharing_date"]'), settlementDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="entitas_inv_no"]'), noInv)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="form_add"]/div[1]/div[11]/div/input'), noInv)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submit-user"]/button'))
WebUI.delay(2)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitSimulasi"]'))

WebUI.delay(20)

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

