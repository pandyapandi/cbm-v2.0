import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

//////Maker Partner create 5 invoice
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userMakerPartner)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.anchor2)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.paymentSCFAP)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

Date today = new Date()

String currentDate = today.format('dd-MM-yyyy')

String invDate = today.format('ddMMyyyy')

String invTime = today.format('hhmmss')

settle = (today + 1)

sharing = (today + 1)

String settlementDate = settle.format('dd-MM-yyyy')

String sharinglimitDate = settle.format('dd-MM-yyyy')

String randomString = org.apache.commons.lang.RandomStringUtils.random(8 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String randomString1 = org.apache.commons.lang.RandomStringUtils.random(8 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String randomString2 = org.apache.commons.lang.RandomStringUtils.random(8 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String randomString3 = org.apache.commons.lang.RandomStringUtils.random(8 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String randomString4 = org.apache.commons.lang.RandomStringUtils.random(8 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String noInv = 'INV-' + randomString

String noInv1 = 'INV-' + randomString1

String noInv2 = 'INV-' + randomString2

String noInv3 = 'INV-' + randomString3

String noInv4 = 'INV-' + randomString4

//String InvNo = (currentDate + '-') + currentTime
//String noInv = (('SCFAP' + invDate) + '-') + invTime
println(noInv)

println(noInv1)

println(noInv2)

println(noInv3)

println(noInv4)

println(currentDate)

println(settlementDate)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(findTestObject('null'), noInv)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

/////////////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.anchor2)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.paymentSCFAP)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(findTestObject('null'), noInv1)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

/////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.anchor2)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.paymentSCFAP)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(findTestObject('null'), noInv2)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

/////////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.anchor2)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.paymentSCFAP)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(findTestObject('null'), noInv3)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

///////////////////////////////////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.anchor2)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.paymentSCFAP)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(findTestObject('null'), noInv4)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

///////////////////////////////
///CHECKER Partner APPROVE
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userCheckerPartner)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

/////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

///////////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

/////////////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

///////////////////////////////////////////////////
////Signer Partner approve
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userSignerPartner)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

/////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

//////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

//////////////////////////////////////
//////Maker Inbox
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userMakerAnchor)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

//////////////////////////////////////
WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

////////////////////////////////////
//// Checker INBOX 
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userCheckerAnchor)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Ok')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

//////////////////////////////////////////
/////////REJECT TIMEEE
///////////////CHECKER PARTNER
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userCheckerPartner)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon Checker Partner Reject')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('Reject successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

//////////////////SIGNER PARTNER
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userSignerPartner)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'SignerPartner Reject')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('Reject successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

///////////////////////////////// maker reject
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userMakerAnchor)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Reject')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('Reject Successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

///////////////////checker reject
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userCheckerAnchor)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Rejected')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('Reject successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

/////////////////////////////////////////SIGNER REJECT
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userSignerAnchor)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Reject')

WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('Reject successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('Close Notification'), [:], FailureHandling.OPTIONAL)

WebUI.closeBrowser()

