import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class login {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userSignerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)

WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a')) //AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0305"]')) //Invoice checker
WebUI.click(log.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a')) //details

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[2]/div[2]/div[2]/button[2]')) //approve
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'ok')
WebUI.setText(log.getTestObjectWithXpath('//*[@id="otp_code"]'), GlobalVariable.otpCode)
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[2]/button[2]')) //submit

WebUI.delay(20)

if(WebUI.verifyTextPresent('successful', true, FailureHandling.CONTINUE_ON_FAILURE)){


WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	println("---------success-------------------")
	
	}else {
	WebUI.takeScreenshot()
	WebUI.click(login.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[3]/div[2]/div[2]/button[1]'))
	WebUI.setText(login.getTestObjectWithXpath('//*[@id="modal_reject_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(login.getTestObjectWithXpath('//*[@id="reject_signer"]'))
	println("---------invoice rejected")
	WebUI.delay(1)

WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	}


