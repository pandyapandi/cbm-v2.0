import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class login {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.openBrowser('')
WebUI.navigateToUrl('http://172.18.133.135:82/CBM/signin')
//WebUI.verifyElementPresent(login.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), 20)
WebUI.setText(login.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'),
	GlobalVariable.userSignerAnchor)

WebUI.setText(login.getTestObjectWithXpath('//*[@id="identk"]'),
	GlobalVariable.password)
WebUI.click(login.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

if(WebUI.verifyTextPresent('oasdkaosdk', true, FailureHandling.OPTIONAL)){
	
	WebUI.click(findTestObject('null'))
	WebUI.setText(findTestObject('null'), 'Katalon Ok')
	WebUI.setText(findTestObject('null'), GlobalVariable.otpCode)
	WebUI.click(findTestObject('null'))
	println("---------success-------------------")
	
	}else {
	WebUI.takeScreenshot()
	WebUI.click(login.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div[3]/div[2]/div[2]/button[2]'))
	WebUI.setText(login.getTestObjectWithXpath('//*[@id="modal_reject_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(login.getTestObjectWithXpath('//*[@id="reject_signer"]'))
	println("---------invoice rejected")
	
	}