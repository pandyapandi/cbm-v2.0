import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.callTestCase(findTestCase('Backlog/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

TestObject checkList1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div[2]/table/tbody/tr[1]/td[1]/input')

TestObject checkList2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div[2]/table/tbody/tr[2]/td[1]/input')

TestObject checkList3 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div[2]/table/tbody/tr[3]/td[1]/input')

TestObject checkList4 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div[2]/table/tbody/tr[4]/td[1]/input')

TestObject createBatch = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/button[@data-target=\'#modal_approve_checker_confirm\']')

TestObject noteChecker = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/textarea[@name=\'note_approve\']')

TestObject countInv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/font[@id=\'totalRecord\']')

TestObject createBatchbutton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/button[@type=\'submit\']')

TestObject closeAlert = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/span[@class=\'close\']')

TestObject reload = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/button[@onclick=\'location.reload()\']')

TestObject approveBatchbutton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[4]/div/div/div/button[2]')

TestObject noteApprovebatch = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[5]/div/div/form/div[1]/div/div[2]/textarea')

TestObject approveBatchbutton1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[5]/div/div/form/div[2]/button[2]')

TestObject approved1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[1]/td[9]')

TestObject approved2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[2]/td[9]')

TestObject approved3 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[3]/td[9]')

TestObject approved4 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/fieldset/div/div/div/div[3]/div[2]/table/tbody/tr[4]/td[9]')

WebUI.click(checkList1)

WebUI.click(checkList2)

WebUI.click(checkList3)

WebUI.click(checkList4)

WebUI.click(createBatch)

WebUI.setText(noteChecker, 'Auto Create Batch checker')

WebUI.verifyTextPresent('Total Record : ', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(countInv, '4', FailureHandling.STOP_ON_FAILURE)

WebUI.click(createBatchbutton)

WebUI.verifyTextPresent('Insert Batch Data Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(closeAlert)

WebUI.click(reload)

WebUI.verifyTextPresent('Mass Approve Verification Result', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Created By', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Checker Comp 19', true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(approveBatchbutton)

WebUI.verifyTextPresent('Process Confirmation', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Disclaimer', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Approver sudah melakukan proses verifikasi kelengkapan dokumen invoice dan dokumen pendukungnya per masing-masing invoice.', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Approver menjamin bahwa data pada daftar pengajuan telah sesuai dengan data pada dokumen invoice.', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Approver akan bertanggung jawab penuh atas kesalahan data invoice dan resiko yang akan muncul di kemudian hari.', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Approver yakin untuk melakukan approval untuk semua invoice yang dipilih dengan melakukan klik tombol', true, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(noteApprovebatch, 'Auto Approve Batch checker')

WebUI.click(approveBatchbutton1)

WebUI.verifyTextPresent('Success. Verification On Progress', true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(closeAlert)

WebUI.delay(15)

WebUI.click(reload)

WebUI.verifyElementText(approved1, 'Approved.', FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(approved2, 'Approved.', FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(approved3, 'Approved.', FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(approved4, 'Approved.', FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Backlog/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

