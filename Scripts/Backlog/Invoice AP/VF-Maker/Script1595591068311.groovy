import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.callTestCase(findTestCase('Backlog/Login Maker'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('null'))

//for (int i = 0; i < 10; i++) {

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

//---- Pengisian Form Create Invoice SCF AR -----//
TestObject clickCompany = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'select2-entitas_partner-container\']')

TestObject inputCompany = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/span[2]/span/span[1]/input')

WebUI.delay(2)

TestObject clickPaymentMethod = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@title=\'- Select Payment Method -\']')

TestObject inputPaymentMethod = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@class=\'select2-search__field\']')

TestObject submitButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@onclick=\'bukaConfirm()\']')

TestObject rejectDisbinfo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[2]/div[2]/div[2]/div/div/div/form/div[3]/div/div/div[3]/div/div/button[1]')

TestObject acceptDisbinfo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'submitSimulasi\']')

TestObject sharingLimit = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id="sharing_date"]')

TestObject profile = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@class=\'nav-item dropdown dropdown-user\']')

TestObject logoutDropDown = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@data-target=\'#modal_logout\']')

TestObject logoutPopUp = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@href=\'http://172.18.133.135:82/CBM/logout\']')

WebUI.click(clickCompany)

WebUI.setText(inputCompany, GlobalVariable.partner)

WebUI.sendKeys(inputCompany, Keys.chord(Keys.DOWN))

WebUI.sendKeys(inputCompany, Keys.chord(Keys.ENTER))

WebUI.click(clickPaymentMethod)

WebUI.setText(inputPaymentMethod, GlobalVariable.vendorFinancing)

WebUI.sendKeys(inputPaymentMethod, Keys.chord(Keys.DOWN))

WebUI.sendKeys(inputPaymentMethod, Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), GlobalVariable.amount)

Date today = new Date()

String currentTime = today.format('hh:mm:ss')

String currentDate = today.format('dd-MM-yyyy')

String invDate = today.format('ddMMyyyy')

String invTime = today.format('hhmmss')

settle = (today + 3)

sharing = (today + 2)

String settlementDate = settle.format('dd-MM-yyyy')

String sharinglimitDate = settle.format('dd-MM-yyyy')

//String InvNo = (currentDate + '-') + currentTime
String noInv = (('VALPUT-VF-SINGLE-AUTO' + invDate) + '-') + invTime

println(noInv)

println(currentDate)

println(settlementDate)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.setText(findTestObject('null'), settlementDate)

WebUI.setText(sharingLimit, sharinglimitDate)

WebUI.setText(findTestObject('null'), noInv)

WebUI.setText(findTestObject('null'), 'AP')

WebUI.delay(3)

WebUI.click(submitButton)

WebUI.delay(3)

WebUI.click(rejectDisbinfo)

WebUI.click(submitButton)

WebUI.delay(3)

WebUI.click(acceptDisbinfo)

WebUI.delay(3)

//}

WebUI.callTestCase(findTestCase('Backlog/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

