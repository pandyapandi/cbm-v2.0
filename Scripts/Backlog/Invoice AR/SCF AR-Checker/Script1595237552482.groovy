import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

//WebUI.navigateToUrl('http://172.18.133.135:82/CBM/signin/')
//
//WebUI.click(findTestObject('null'))
//
//WebUI.setText(findTestObject('null'), 'makercbm2020@mailsac.com')
//
//WebUI.setEncryptedText(findTestObject('null'), '/5S6MFFLcE4mlsixtc6/Tg==')
//
//WebUI.click(findTestObject('null'))
//
//String noInv
//
//WebUI.callTestCase(findTestCase('Backlog/Invoice AR/SCF AR-Maker'), [('noInv') : noInv], FailureHandling.STOP_ON_FAILURE)
////WebUI.callTestCase(findTestCase('Backlog/Login Maker'), [('username') : GlobalVariable.usernameChecker], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl('http://172.18.133.135:82/CBM/signin/')

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'checkercbm2020@mailsac.com')

WebUI.setEncryptedText(findTestObject('null'), '/5S6MFFLcE4mlsixtc6/Tg==')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.delay(2)

WebUI.click(findTestObject('null'))

String no_inv = 'AUTO-SCF AR-23072020-111220'

String no_ref = '0001920000729'

WebUI.setText(findTestObject('null'), no_inv)

WebUI.delay(1)

//WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))
//TestObject checkbox = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=//label[.="AUTO-SCF AR-23072020-111220"]/@for]')
//TestObject referenceNo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/span/font/')
//*[@id="row_6"]/td[1]
//input[.//following-sibling::label[.='Inv No :'+no_inv]]
//ref_no = WebUI.getAttribute(findTestObject('Object Repository/Page_Corporate Billing Management/font_Ref No : 0001920000729'), 'value')
//ref_no = WebUI.getAttribute(referenceNo, 'value')
//ref_no = WebUI.getText(referenceNo)
//print ref_no
//html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[3]/span/font
WebUI.check(checkbox)

WebUI.openBrowser('')

//WebUI.navigateToUrl('http://172.18.133.135:82/CBM/signin/')
//
//WebUI.click(findTestObject('null'))
//
//WebUI.setText(findTestObject('null'), 'makercbm2020@mailsac.com')
//
//WebUI.setEncryptedText(findTestObject('null'), '/5S6MFFLcE4mlsixtc6/Tg==')
//
//WebUI.click(findTestObject('null'))
//
//String noInv
//
//WebUI.callTestCase(findTestCase('Backlog/Invoice AR/SCF AR-Maker'), [('noInv') : noInv], FailureHandling.STOP_ON_FAILURE)
////WebUI.callTestCase(findTestCase('Backlog/Login Maker'), [('username') : GlobalVariable.usernameChecker], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl('http://172.18.133.135:82/CBM/signin/')

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'checkercbm2020@mailsac.com')

WebUI.setEncryptedText(findTestObject('null'), 
    '/5S6MFFLcE4mlsixtc6/Tg==')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.delay(2)

WebUI.click(findTestObject('null'))

String no_inv = 'AUTO-SCF AR-23072020-111220'

String no_ref = '0001920000729'

WebUI.setText(findTestObject('null'), no_inv)

WebUI.delay(1)

//WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))
//TestObject checkbox = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=//label[.="check_"+no_ref]/@for]')

String check_id="check_"+no_ref

TestObject checkbox = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=check_id]')

//TestObject referenceNo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/span/font/')
//*[@id="row_6"]/td[1]
//input[.//following-sibling::label[.='Inv No :'+no_inv]]
//ref_no = WebUI.getAttribute(findTestObject('Object Repository/Page_Corporate Billing Management/font_Ref No : 0001920000729'), 'value')
//ref_no = WebUI.getAttribute(referenceNo, 'value')
//ref_no = WebUI.getText(referenceNo)
//print ref_no
//html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div[1]/form/div[1]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[3]/span/font
WebUI.check(checkbox)

WebUI.verifyElementChecked(checkbox, 3)

WebUI.click(findTestObject('null'))

