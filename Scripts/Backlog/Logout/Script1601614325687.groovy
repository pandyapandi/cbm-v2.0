import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

TestObject User = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/ul/li/a[@data-toggle=\'dropdown\']')

TestObject logoutButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/ul/li/div/a[@data-target=\'#modal_logout\']')

TestObject logoutButton_1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/div/a[@class=\'btn bg-danger\']')

//WebUI.callTestCase(findTestCase('Backlog/Login Maker'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(User)

WebUI.verifyTextPresent("My Profile", true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent("Company Profile", true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent("Change password", true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent("Logout", true, FailureHandling.STOP_ON_FAILURE)

WebUI.click(logoutButton)

WebUI.click(logoutButton_1)

println('Logout berhasil')

//WebUI.closeBrowser()

