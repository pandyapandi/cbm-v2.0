import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

// Test Object
TestObject username = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/input[@name=\'ident\']')

TestObject password = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/input[@name=\'identk\']')

TestObject loginButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/button[@id=\'submitsignin\']')

TestObject Announcement = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[3]/div/div/div[3]/button')

TestObject announcementButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/a/button')

TestObject showPass = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div/span/input[@id=\'showPass\']')

TestObject regisformButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[1]/div/div/div[2]/div[2]/div/div[1]/a/button')

TestObject regisformClosebutton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[3]/button')

TestObject formcbm01 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[2]/ul/li[1]/a')

TestObject formcbm02 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[2]/ul/li[2]/a')

TestObject formcbm03 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[2]/ul/li[3]/a')

TestObject formcbm04 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[2]/ul/li[4]/a')

TestObject formcbm05 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div[2]/div/div/div[2]/ul/li[5]/a')

WebUI.click(Announcement)

//Registration Form
WebUI.click(regisformButton)

WebUI.verifyElementClickable(formcbm01 //(CBM-01) Formulir Registrasi
    )

WebUI.verifyElementClickable(formcbm02 //(CBM-02) Formulir Syarat dan Ketentuan
    )

WebUI.verifyElementClickable(formcbm03 //(CBM-03) Formulir Update Admin, Sysadm dan Reviewer
    )

WebUI.verifyElementClickable(formcbm04 //(CBM-05a) Formulir Update Payment Method Anchor
    )

WebUI.verifyElementClickable(formcbm05 //(CBM-05b) Formulir Update Payment Method Partner
    )

WebUI.click(regisformClosebutton)

//Announcement Button
WebUI.click(announcementButton)

WebUI.verifyTextPresent('Announcement', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(Announcement)

//Invalid username
println(((((('Negative Test Case' + '\r\n') + 'Username: ') + GlobalVariable.invalidUsername) + '\r\n') + 'Password: ') + 
    GlobalVariable.password)

WebUI.setText(username, GlobalVariable.invalidUsername)

WebUI.setText(password, GlobalVariable.password)

WebUI.click(showPass)

WebUI.click(loginButton)

WebUI.click(Announcement)

WebUI.verifyTextPresent('FAILED! Username and password not match.', true, FailureHandling.STOP_ON_FAILURE)

//Invalid Password
println(((((('Negative Test Case' + '\r\n') + 'Username: ') + GlobalVariable.usernameMaker) + '\r\n') + 'Password: ') + 
    GlobalVariable.invalidPassword)

WebUI.setText(username, GlobalVariable.usernameMaker)

WebUI.setText(password, GlobalVariable.invalidPassword)

WebUI.click(showPass)

WebUI.click(loginButton)

WebUI.click(Announcement)

WebUI.verifyTextPresent('FAILED! Username and password not match.', true, FailureHandling.STOP_ON_FAILURE)

//Valid Login
println(((((('Positive Test Case' + '\r\n') + 'Username: ') + GlobalVariable.usernameMaker) + '\r\n') + 'Password: ') + 
    GlobalVariable.password)

WebUI.setText(username, GlobalVariable.usernameMaker)

WebUI.setText(password, GlobalVariable.password)

WebUI.click(showPass)

WebUI.verifyElementText(loginButton, 'Login', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(loginButton)

WebUI.verifyTextPresent('Dashboard', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Katalon 01', true, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Maker Comp19', true, FailureHandling.STOP_ON_FAILURE)

println('Login berhasil dengan akun ' + GlobalVariable.usernameMaker)

