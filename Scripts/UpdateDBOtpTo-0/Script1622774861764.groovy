import java.sql.ResultSet as ResultSet
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import dbConnection.DBConnection as DBConnection
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

WebUI.delay(1)
// select before update
ResultSet rs = DBConnection.execDBQuery("update setting s set s.values='0' where parameter='OTP'")
rs.next()

DBConnection.closeMysqlConn()

//String Rescolumn = rs.getString("values")

//println("Values Captcha from table setting : "+ Rescolumn)

// update and select after update
/*
ResultSet rs = DBConnection.execDBQuery("update setting s set s.values='0' where parameter='OTP'")

ResultSet rs = DBConnection.execDBQuery("select s.values from setting s where parameter='OTP'")

String Rescolumn = rs.getString("values")

println("Values from table setting : "+ Rescolumn)
*/



