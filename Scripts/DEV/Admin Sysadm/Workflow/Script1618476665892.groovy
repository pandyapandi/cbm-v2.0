import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('null'))
/////////////////////////////////////////////////////////////////////
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userAdminAnchor)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 
    'Approval')
WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), 'OK Katalon')
WebUI.click(findTestObject('null'))
WebUI.acceptAlert()
WebUI.verifyTextPresent('Success', false, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 
    'confirmation')
WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), 'OK Katalon')
WebUI.click(findTestObject('null'))
WebUI.acceptAlert()
WebUI.verifyTextPresent('Success', true, FailureHandling.OPTIONAL)
WebUI.delay(2)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
//////////////////////////////////Sysadm Approve Achor
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 
    GlobalVariable.userSysadmAnchor)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 'ok')
WebUI.click(findTestObject('null'))
WebUI.verifyTextPresent('success', true, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 'ok')
WebUI.click(findTestObject('null'))
WebUI.verifyTextPresent('success', true, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))

///////////////////////////////////////////////////////////////////////////////////////////////////
///////Partner
WebUI.click(findTestObject('null'))
/////////////////////////////////////////////////////////////////////
WebUI.setText(findTestObject('null'),
	GlobalVariable.userAdminPartner)
WebUI.setText(findTestObject('null'),
	GlobalVariable.password)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'),
	'Approval')
WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), 'OK Katalon')
WebUI.click(findTestObject('null'))
WebUI.acceptAlert()
WebUI.verifyTextPresent('Success', true, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'),
	'confirmation')
WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), '1')
WebUI.setText(findTestObject('null'), 'OK Katalon')
WebUI.click(findTestObject('null'))
WebUI.acceptAlert()
WebUI.verifyTextPresent('Success', true, FailureHandling.OPTIONAL)
WebUI.delay(2)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'),
	GlobalVariable.userSysadmPartner)
WebUI.setText(findTestObject('null'),
	GlobalVariable.password)
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 'ok')
WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('success', true, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.setText(findTestObject('null'), 'ok')
WebUI.click(findTestObject('null'))

WebUI.verifyTextPresent('success', true, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('null'))

WebUI.closeBrowser()
