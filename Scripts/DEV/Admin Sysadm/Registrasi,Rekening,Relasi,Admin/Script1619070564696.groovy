import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class closeNotification {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class username {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class password {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class btn {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class relation {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class setupPartner {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class selectCompany {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class ceklis {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class komentar {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class profile {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class cari {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class detail {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class test {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class companyManagement {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class search {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class details {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class note {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class setupAnchor {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}

String randomString = org.apache.commons.lang.RandomStringUtils.random(4 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String randomString1 = org.apache.commons.lang.RandomStringUtils.random(4 /*Jumlah*/ , true /*Alfabet*/ , true /*Number*/ )

String namaCompanyAnchor = 'KAT-ANCHOR-' + randomString

String namaCompanyPartner = 'KAT-PARTNER-' + randomString1

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.makerOSR)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'incorporated')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'PT')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), randomString)

Date today = new Date()

String currentDate = today.format('dd-MM-yyyy')

String invDate = today.format('ddMMyyyy')

String invTime = today.format('hhmmss')

settle = (today + 1)

sharing = (today + 1)

String settlementDate = settle.format('dd-MM-yyyy')

String sharinglimitDate = settle.format('dd-MM-yyyy')

//String InvNo = (currentDate + '-') + currentTime
String noInv = (('KATALON' + invDate) + '-') + invTime

println(noInv)

println(currentDate)

println(settlementDate)

WebUI.setText(findTestObject('null'), currentDate)

WebUI.sendKeys(findTestObject('null'), Keys.chord(
        Keys.ENTER))

WebUI.setText(findTestObject('null'), currentDate)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), randomString)

WebUI.setText(findTestObject('null'), '123')

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'DKI Jakarta')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'KOTA JAKARTA SELATAN')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'CILANDAK')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), 'Jl.')

WebUI.setText(findTestObject('null'), 'Ok')

WebUI.click(findTestObject('null'))

WebUI.delay(2)

/////////////////////////////////////////////////////////////////

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'incorporated')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'PT')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), randomString1)


WebUI.setText(findTestObject('null'), currentDate)

WebUI.sendKeys(findTestObject('null'), Keys.chord(
		Keys.ENTER))

WebUI.setText(findTestObject('null'), currentDate)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), randomString1)

WebUI.setText(findTestObject('null'), '123')

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'DKI Jakarta')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'KOTA JAKARTA SELATAN')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'CILANDAK')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), 'Jl.')

WebUI.setText(findTestObject('null'), 'Ok')

WebUI.click(findTestObject('null'))

WebUI.delay(2)

////////////////////////////LOGOUT MAKER OSR

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.signerOSR)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'ok')

WebUI.click(findTestObject('null'))

WebUI.delay(2)
//////////////////////////////////////////////

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'ok')

WebUI.click(findTestObject('null'))

WebUI.delay(2)

//////////////////////////////////////////////LOGOUT SIGNER OSR

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

///////////////////////////////////// ACCOUNT ANCHOR
///////////////////////////////////// LOGIN MAKER
/////////////////////////////////////

WebUI.setText(findTestObject('null'),
	GlobalVariable.makerOSR)

WebUI.setText(findTestObject('null'),
	GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//1
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//2
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//3
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//4
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//5
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//6
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//7
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//8
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//9
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//10
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//11

WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening1A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening2A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening3A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening4A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening5A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening6A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening7A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening8A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening9A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input10"]'),
	GlobalVariable.Rekening10A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input11"]'),
	GlobalVariable.Rekening11A, false)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening1B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening2B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening3B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening4B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening5B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening6B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening7B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening8B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening9B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input10"]'), 
	GlobalVariable.Rekening10B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input11"]'),
	GlobalVariable.Rekening11B)

WebUI.setText(note.getTestObjectWithXpath('//*[@id="0"]/td[5]/input'), GlobalVariable.Rekening1C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="1"]/td[5]/input'), GlobalVariable.Rekening2C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="2"]/td[5]/input'), GlobalVariable.Rekening3C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="3"]/td[5]/input'), GlobalVariable.Rekening4C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="4"]/td[5]/input'), GlobalVariable.Rekening5C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="5"]/td[5]/input'), GlobalVariable.Rekening6C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="6"]/td[5]/input'), GlobalVariable.Rekening7C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="7"]/td[5]/input'), GlobalVariable.Rekening8C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="8"]/td[5]/input'), GlobalVariable.Rekening9C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="9"]/td[5]/input'), GlobalVariable.Rekening10C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="10"]/td[5]/input'), GlobalVariable.Rekening11C)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.delay(2)

///////////////////////////////////////////////////ACCOUNT PARTNER

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//1
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//2
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//3
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//4
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//5
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//6
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//7
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//8
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//9
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//10
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//11

WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening1A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening2A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening3A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening4A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening5A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening6A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening7A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening8A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening9A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input10"]'),
	GlobalVariable.Rekening10A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input11"]'),
	GlobalVariable.Rekening11A, false)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening1B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening2B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening3B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening4B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening5B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening6B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening7B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening8B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening9B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input10"]'), 
	GlobalVariable.Rekening10B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input11"]'),
	GlobalVariable.Rekening11B)

WebUI.setText(note.getTestObjectWithXpath('//*[@id="0"]/td[5]/input'), GlobalVariable.Rekening1C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="1"]/td[5]/input'), GlobalVariable.Rekening2C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="2"]/td[5]/input'), GlobalVariable.Rekening3C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="3"]/td[5]/input'), GlobalVariable.Rekening4C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="4"]/td[5]/input'), GlobalVariable.Rekening5C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="5"]/td[5]/input'), GlobalVariable.Rekening6C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="6"]/td[5]/input'), GlobalVariable.Rekening7C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="7"]/td[5]/input'), GlobalVariable.Rekening8C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="8"]/td[5]/input'), GlobalVariable.Rekening9C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="9"]/td[5]/input'), GlobalVariable.Rekening10C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="10"]/td[5]/input'), GlobalVariable.Rekening11C)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.delay(2)

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

///////////////////////////////////////// ACC ACOUNT ANCHOR

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
 
WebUI.setText(findTestObject('null'), GlobalVariable.signerOSR)
 
WebUI.setText(findTestObject('null'), GlobalVariable.password)
 
WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))

WebUI.click(companyManagement.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))
 
WebUI.click(companyManagement.getTestObjectWithXpath('//*[@id="0903"]'))
 
WebUI.setText(search.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), namaCompanyAnchor)
 
WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))
 
WebUI.click(details.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))
 
WebUI.setText(search.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'OK')
 
WebUI.click(details.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))
 
WebUI.acceptAlert(FailureHandling.OPTIONAL)
 
WebUI.delay(2)
 
////////////////////////ACC rekening Partner
 
WebUI.click(companyManagement.getTestObjectWithXpath('//*[@id="0903"]'))
  
WebUI.setText(search.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), namaCompanyPartner)
  
WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))
  
WebUI.click(details.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))
  
WebUI.setText(search.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'OK')
  
WebUI.click(details.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))
  
WebUI.acceptAlert(FailureHandling.OPTIONAL)
  
WebUI.delay(2)
  
////////////////////logout signerOSR

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
   
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
   
WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////RELASI ANCHOR PARTNER
////////////////////////////////////////////////////////ANCHOR

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
 WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.makerOSR)
 WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 ///////////////////////////////////////////
 WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))
 WebUI.click(setupAnchor.getTestObjectWithXpath('//*[@id="0701"]'))
 WebUI.click(setupAnchor.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/a'))
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-dropdown_company-container"]'))
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 ///////////////////////
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="AP"]'))
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="AR"]'))
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="ARK"]'))
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="DF"]'))
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="VA"]'))
 WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="VF"]'))
 WebUI.setText(komentar.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[3]/div/textarea'), 'OK')
 WebUI.click(btn.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[4]/button'))
 
 WebUI.delay(1)
 WebUI.verifyTextPresent('successful', true, FailureHandling.OPTIONAL)
 WebUI.delay(8)
 ///////////////////
 WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
 
 //////////////////////////////////////////////////////////////////////////Signer app relasi Anchor
 
 WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
 WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.signerOSR)
 WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 WebUI.click(test.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[5]/a'))
 WebUI.click(test.getTestObjectWithXpath('//*[@id="0702"]'))
 WebUI.setText(cari.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), namaCompanyAnchor)
 WebUI.click(detail.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[6]/div/a'))
 WebUI.click(btn.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[8]/button[2]'))
 WebUI.setText(komentar.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div/div/textarea'), 'ok')
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))
 
 WebUI.delay(1)
 WebUI.verifyTextPresent('successful', true, FailureHandling.OPTIONAL)
 WebUI.delay(8)
 
 
 WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
 
 ////////////////////////////////////////////////////////////   PARTNER
 /////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////
 
 WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
  
 WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.makerOSR)
 
 WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 
 ///////////////////////////////////////////
 WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0703"]'))
 
 WebUI.click(setupPartner.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/a'))
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-partner_id-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyPartner //selectSetupPartner
	 )
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 ///////////////////////////////////////////////SCF AP Achor-Partner Without
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AP')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening8B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //partnerupload
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="partner_upload_yes"]'))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////////////////////////SCF AR Anchor-Partner without
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AR')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //settlementType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

 ///////////////////////////////////////////////SCF AR Anchor-Partner with
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.anchor3)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AR')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //settlementType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'With')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

 ///////////////////////////////////////////////SCF AR Anchor-Anchor
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AR')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //settlementType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////////////////////////VF
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'VF')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////EscrowAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-escrow_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening3B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////DisbursAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening10B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //settlementType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening3B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //InterestType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="interest_sharing_a"]'))
 
 ///MaturityDays
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days"]'), '180')
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //partnerUpload
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="partner_upload_yes"]'))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////////////////////////DF without
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'DF')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////DisbursAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening10B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening3B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 ///MaturityDays
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days"]'), '1')
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////////////////////////DF with
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.anchor3)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'DF')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////DisbursAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening10B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening3B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 ///MaturityDays
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days"]'), '1')
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'With')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

 ///////////////////////////////////////////////ARK
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'ARK')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////EscrowAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-escrow_account_input-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening9B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //////beneficiaryAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input_dfark-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////DisbursAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input_dfark-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening10B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input_dfark-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening3B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 ///MaturityDays
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days_dfark"]'), '1')
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input_dfark-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[3]/div/textarea'),
	 'OK')
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="btn_sbmt"]'))
 
 WebUI.verifyTextPresent('successful', false, FailureHandling.CONTINUE_ON_FAILURE)

 /////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////// self invoicing
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0703"]'))
 
 WebUI.click(setupPartner.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/a'))
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-partner_id-container"]'))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor //selectSetupPartner
	 )
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 ///////////////////////////////////////////////SCF AP 
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AP')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening8B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //partnerupload
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="partner_upload_yes"]'))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))
 
 ///////////////////////////////////////////////SCF AR self invoicing
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	 )
 
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), namaCompanyAnchor)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////paymentmethod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AR')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////beneficiaryaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //settlementType
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))
 
 //////settlementaccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.Rekening4B)
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //////gracePeriod
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')
 
 //confirmation
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]')
	 )
 
 WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')
 
 WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
 
 //submitAccount
 WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

///////////////////////////////////////////////
 
 WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
 
 ////////////////////////////////////////////////////////SIGNER
 
 WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
  
 WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.signerOSR)
  
 WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
  
 WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
  
 ///////////////////////////////////////////
 WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[5]/a'))
  
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0704"]'))
  
 WebUI.setText(username.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), namaCompanyPartner)
  
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[6]/div/a'))
  
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[8]/button[2]'))
 
 WebUI.setText(username.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div/div/textarea'), 'ok')
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))
 
 //////////////////////////////////////////////
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0704"]'))
  
 WebUI.setText(username.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), namaCompanyAnchor)
  
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[6]/div/a'))
  
 WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[8]/button[2]'))
 
 WebUI.setText(username.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div/div/textarea'), 'ok')
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))
 
 ///////////////////////////////////////////////
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
 
 WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
 

/////////////////////////////////////////// new account ADMIN
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////


public class companyM {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

public class detailApprove {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class companies {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class ApproveMA {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class noteMA {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class approveMA2 {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class role {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class ID {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}


String namaAdminAnchor = 'Admin-' + randomString
String namaSysadmAnchor = 'Sysadmin-' + randomString
String namaReviewerAnchor = 'Reviewer-' + randomString
String namaAdminPartner = 'Admin-' + randomString1
String namaSysadmPartner = 'Sysadmin-' + randomString1
String namaReviewerPartner = 'Reviewer-' + randomString1

String emailAdminAnchor = ('Admin_' + randomString) + '@mailsac.com'
String emailSysadmAnchor = ('Sysadmin_' + randomString) + '@mailsac.com'
String emailReviewerAnchor = ('Reviewer_' + randomString) + '@mailsac.com'
String emailAdminPartner = ('Admin_' + randomString1) + '@mailsac.com'
String emailSysadmPartner = ('Sysadmin_' + randomString1) + '@mailsac.com'
String emailReviewerPartner = ('Reviewer_' + randomString1) + '@mailsac.com'

String urlAdminAnchor = 'https://mailsac.com/inbox/' + emailAdminAnchor
String urlSysadmAnchor = 'https://mailsac.com/inbox/' + emailSysadmAnchor
String urlReviewerAnchor = 'https://mailsac.com/inbox/' + emailReviewerAnchor
String urlAdminPartner = 'https://mailsac.com/inbox/' + emailAdminPartner
String urlSysadmPartner = 'https://mailsac.com/inbox/' + emailSysadmPartner
String urlReviewerPartner = 'https://mailsac.com/inbox/' + emailReviewerPartner


WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

WebUI.setText(findTestObject('null'),
	GlobalVariable.makerOSR)

WebUI.setText(findTestObject('null'),
	GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////admin new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaAdminAnchor)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'Admin')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Admin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailAdminAnchor)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////sysadmin new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaSysadmAnchor)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'SysAdmin')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-SysAdmin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailSysadmAnchor)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////reviewer new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaReviewerAnchor)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'Reviewer')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Reviewer')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailReviewerAnchor)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))


//////////////////////////////////////////////////////////////////newAdminPartner

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaAdminPartner)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'Admin')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Admin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailAdminPartner)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////sysadmin new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaSysadmPartner)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'SysAdmin')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-SysAdmin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailSysadmPartner)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////reviewer new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), namaReviewerPartner)

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

WebUI.setText(findTestObject('null'),
	'Reviewer')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Reviewer')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), emailReviewerPartner)

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))


/////////////////////////////////////////////////////////////////

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//////////////////////////////////////////////// ACC SIGNER

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

WebUI.setText(findTestObject('null'), GlobalVariable.signerOSR)

WebUI.setText(findTestObject('null'), GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))
 
WebUI.click(companyM.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a/span'))

/////////////////////////////////////////////////////////SIGNER OSR APP ADMIN
 
WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyAnchor)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////SIGNER OSR APP ADMIN

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), namaCompanyPartner)

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////////////////////////////////////
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

WebUI.navigateToUrl(urlAdminAnchor)
WebUI.delay(2)

WebUI.executeJavaScript('window.open();', [])
currentWindow =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow + 1)
WebUI.navigateToUrl(urlSysadmAnchor)
WebUI.delay(2)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlReviewerAnchor)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlAdminPartner)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlSysadmPartner)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlReviewerPartner)

