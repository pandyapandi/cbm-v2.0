import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	GlobalVariable.userAdminAnchor)

WebUI.setText(findTestObject('null'),
	GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String randomString = org.apache.commons.lang.RandomStringUtils.random(6 /*Jumlah*/ , false /*Alfabet*/ , true /*Number*/ )
String randomString1 = org.apache.commons.lang.RandomStringUtils.random(6 /*Jumlah*/ , false /*Alfabet*/ , true /*Number*/ )

String namaMakerAnchor = 'Maker_A' + randomString

String emailMakerAnchor = ('Maker_A' + randomString) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaMakerAnchor)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailMakerAnchor)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

/////////////////////////user checker

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String namaCheckerAnchor = 'Checker_A' + randomString

String emailCheckerAnchor = ('Checker_A' + randomString) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaCheckerAnchor)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailCheckerAnchor)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

/////////////////////////////user signer

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String namaSignerAnchor = 'Signer_A' + randomString

String emailSignerAnchor = ('Signer_A' + randomString) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaSignerAnchor)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailSignerAnchor)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('DEV/Admin Sysadm/dll/Acc User'), [:], FailureHandling.CONTINUE_ON_FAILURE)

////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	GlobalVariable.userAdminPartner)

WebUI.setText(findTestObject('null'),
	GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String namaMakerPartner = 'Maker_P' + randomString1

String emailMakerPartner = ('Maker_P' + randomString1) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaMakerPartner)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailMakerPartner)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

/////////////////////////user checker

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String namaCheckerPartner = 'Checker_P' + randomString1

String emailCheckerPartner = ('Checker_P' + randomString1) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaCheckerPartner)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailCheckerPartner)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

/////////////////////////////user signer

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

String namaSignerPartner = 'Signer_P' + randomString1

String emailSignerPartner = ('Signer_P' + randomString1) + '@mailsac.com'

WebUI.setText(findTestObject('null'), namaSignerPartner)

WebUI.setText(findTestObject('null'), '1111')

WebUI.setText(findTestObject('null'), 'CEO')

WebUI.setText(findTestObject('null'), emailSignerPartner)

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Jl. Katalon')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.setText(findTestObject('null'), '1')

WebUI.setText(findTestObject('null'), '10.000')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.verifyTextPresent('Success', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.callTestCase(findTestCase('DEV/Admin Sysadm/dll/Acc User Partner'), [:], FailureHandling.CONTINUE_ON_FAILURE)

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
import java.awt.Robot
import java.awt.event.KeyEvent
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String urlMakerAnchor = 'https://mailsac.com/inbox/' + emailMakerAnchor
String urlCheckerAnchor = 'https://mailsac.com/inbox/' + emailCheckerAnchor
String urlSignerAnchor = 'https://mailsac.com/inbox/' + emailSignerAnchor

String urlMakerPartner = 'https://mailsac.com/inbox/' + emailMakerPartner
String urlCheckerPartner = 'https://mailsac.com/inbox/' + emailCheckerPartner
String urlSignerPartner = 'https://mailsac.com/inbox/' + emailSignerPartner

WebUI.navigateToUrl(urlMakerAnchor)
WebUI.delay(2)

WebUI.executeJavaScript('window.open();', [])
currentWindow =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow + 1)
WebUI.navigateToUrl(urlCheckerAnchor)
WebUI.delay(2)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlSignerAnchor)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlMakerPartner)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlCheckerPartner)

WebUI.executeJavaScript('window.open();', [])
currentWindow1 =  WebUI.getWindowIndex()
WebUI.switchToWindowIndex(currentWindow1 + 1)
WebUI.navigateToUrl(urlSignerPartner)
