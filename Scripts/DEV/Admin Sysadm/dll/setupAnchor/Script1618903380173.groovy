import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
public class closeNotification {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class username {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class password {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class btn {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class relation {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class setupAnchor {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class selectCompany {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class ceklis {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class komentar {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class profile {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class cari {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class detail {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class test {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}
public class setupPartner {public static TestObject getTestObjectWithXpath(String xpath) {return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)}}

WebUI.openBrowser('')
WebUI.maximizeWindow()
WebUI.navigateToUrl(GlobalVariable.url)
WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.makerOSR)
WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
///////////////////////////////////////////
WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))
WebUI.click(setupAnchor.getTestObjectWithXpath('//*[@id="0701"]'))
WebUI.click(setupAnchor.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/a'))
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-dropdown_company-container"]'))
WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')
WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
///////////////////////
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="AP"]'))
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="AR"]'))
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="ARK"]'))
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="DF"]'))
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="VA"]'))
WebUI.click(ceklis.getTestObjectWithXpath('//*[@id="VF"]'))
WebUI.setText(komentar.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[3]/div/textarea'), 'OK')
WebUI.click(btn.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/form/div[4]/button'))

WebUI.delay(1)
WebUI.verifyTextPresent('successful', true, FailureHandling.OPTIONAL)
WebUI.delay(8)
///////////////////
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//////////////////////////////////////////////////////////////////////////

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.signerOSR)
WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
WebUI.click(test.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[5]/a'))
WebUI.click(test.getTestObjectWithXpath('//*[@id="0702"]'))
WebUI.setText(cari.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), '00069')
WebUI.click(detail.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[6]/div/a'))
WebUI.click(btn.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[8]/button[2]'))
WebUI.setText(komentar.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div/div/textarea'), 'ok')
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

WebUI.delay(1)
WebUI.verifyTextPresent('successful', true, FailureHandling.OPTIONAL)
WebUI.delay(8)


WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////////////////////////////////////   PARTNER
/////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
 
WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.makerOSR)

WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)

WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))

///////////////////////////////////////////
WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0703"]'))

WebUI.click(setupPartner.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[1]/div/a'))

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-partner_id-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00070' //selectSetupPartner
	)

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

///////////////////////////////////////////////SCF AP
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	)

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////paymentmethod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AP')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////beneficiaryaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '020601000025307')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////settlementaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]')
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')

//partnerupload
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="partner_upload_yes"]'))

//submitAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

///////////////////////////////////////////////SCF AR
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	)

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////paymentmethod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'SCF AR')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////beneficiaryaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//settlementType
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))

//////settlementaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '020601000025307')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')

//confirmation
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//submitAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

///////////////////////////////////////////////VF
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	)

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////paymentmethod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'VF')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////beneficiaryaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////EscrowAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-escrow_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901000032303')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////DisbursAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '020601501142157')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//settlementType
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="settlement_type_a"]'))

//////settlementaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901000032303')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//InterestType
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="interest_sharing_a"]'))

///MaturityDays
WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days"]'), '180')

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')

//partnerUpload
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="partner_upload_yes"]'))

//submitAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

///////////////////////////////////////////////DF
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	)

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////paymentmethod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'DF')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////beneficiaryaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////DisbursAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '020601501142157')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////settlementaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901000032303')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

///MaturityDays
WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days"]'), '1')

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')

//confirmation
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//submitAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

///////////////////////////////////////////////ARK
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[2]/div/div[1]/div/button') //setupRelation
	)

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-anchor_id_input-container"]') //selectSetupAnchor
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '00069')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////paymentmethod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-feature_type_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'ARK')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////beneficiaryAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////EscrowAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-escrow_account_input-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001200307')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period"]'), '500')

//////beneficiaryAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-receive_account_input_dfark-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901001236308')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////DisbursAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-disbursement_account_input_dfark-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '020601501142157')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//////settlementaccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-settlement_account_input_dfark-container"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), '001901000032303')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

///MaturityDays
WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="maturity_days_dfark"]'), '1')

//////gracePeriod
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), Keys.chord(Keys.DELETE))

WebUI.setText(selectCompany.getTestObjectWithXpath('//*[@id="grace_period_dfark"]'), '500')

//confirmation
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="select2-status_confirm_input_dfark-container"]') 
	)

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), 'Without')

WebUI.sendKeys(selectCompany.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))

//submitAccount
WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="add_anchor"]'))

WebUI.setText(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/form/div[3]/div/textarea'),
	'OK')

WebUI.click(selectCompany.getTestObjectWithXpath('//*[@id="btn_sbmt"]'))

WebUI.verifyTextPresent('successful', false, FailureHandling.CONTINUE_ON_FAILURE)
/////////////////////////////////////////////////////////////////

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btn.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////////////////////////////////SIGNER

WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))
 
WebUI.setText(username.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.signerOSR)
 
WebUI.setText(password.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
 
WebUI.click(btn.getTestObjectWithXpath('//*[@id="submitsignin"]'))
 
///////////////////////////////////////////
WebUI.click(relation.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[5]/a'))
 
WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="0704"]'))
 
WebUI.setText(username.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), '00070')
 
WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[6]/div/a'))
 
WebUI.click(selectCompany.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[8]/button[2]'))

WebUI.setText(username.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div/div/textarea'), 'ok')

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

//////////////////////////////////////////////

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))

WebUI.click(setupPartner.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.closeBrowser()








