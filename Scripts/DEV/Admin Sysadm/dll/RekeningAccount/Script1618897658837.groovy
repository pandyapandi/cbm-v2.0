import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

public class note {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class profile {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class closeNotification {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class companyManagement {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class search {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class details {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.makerOSR)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), '00067')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//1
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//2
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//3
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//4
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//5
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//6
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//7
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//8
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//9
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//10
WebUI.click(note.getTestObjectWithXpath('//*[@id="tambah-row"]'))//11

WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening1A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening2A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening3A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening4A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening5A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening6A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening7A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening8A, false)
WebUI.selectOptionByValue(findTestObject('null'), 
    GlobalVariable.Rekening9A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input10"]'),
	GlobalVariable.Rekening10A, false)
WebUI.selectOptionByValue(note.getTestObjectWithXpath('//*[@id="type_account_input11"]'),
	GlobalVariable.Rekening11A, false)


WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening1B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening2B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening3B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening4B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening5B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening6B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening7B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening8B)
WebUI.setText(findTestObject('null'), 
    GlobalVariable.Rekening9B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input10"]'), 
	GlobalVariable.Rekening10B)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="account_number_input11"]'),
	GlobalVariable.Rekening11B)

WebUI.setText(note.getTestObjectWithXpath('//*[@id="0"]/td[5]/input'), GlobalVariable.Rekening1C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="1"]/td[5]/input'), GlobalVariable.Rekening2C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="2"]/td[5]/input'), GlobalVariable.Rekening3C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="3"]/td[5]/input'), GlobalVariable.Rekening4C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="4"]/td[5]/input'), GlobalVariable.Rekening5C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="5"]/td[5]/input'), GlobalVariable.Rekening6C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="6"]/td[5]/input'), GlobalVariable.Rekening7C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="7"]/td[5]/input'), GlobalVariable.Rekening8C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="8"]/td[5]/input'), GlobalVariable.Rekening9C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="9"]/td[5]/input'), GlobalVariable.Rekening10C)
WebUI.setText(note.getTestObjectWithXpath('//*[@id="10"]/td[5]/input'), GlobalVariable.Rekening11C)
 

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

WebUI.acceptAlert()

WebUI.delay(10)

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////////////////////////
WebUI.click(closeNotification.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'))

WebUI.setText(findTestObject('null'), GlobalVariable.signerOSR)

WebUI.setText(findTestObject('null'), GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))

WebUI.click(companyManagement.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a'))

WebUI.click(companyManagement.getTestObjectWithXpath('//*[@id="0903"]'))

WebUI.setText(search.getTestObjectWithXpath('//*[@id="DataTables_Table_0_filter"]/label/input'), '00066')

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(details.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(search.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'OK')

WebUI.click(details.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

WebUI.acceptAlert(FailureHandling.OPTIONAL)

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))

WebUI.click(profile.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

WebUI.click(findTestObject('null'))

