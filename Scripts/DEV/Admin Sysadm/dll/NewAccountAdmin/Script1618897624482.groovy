import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    GlobalVariable.makerOSR)

WebUI.setText(findTestObject('null'), 
    GlobalVariable.password)

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////admin new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

public class details {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))

//WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Admin-random')

public class role {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))

//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'Admin')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

public class ID {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))

//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 
    'KTP')

WebUI.sendKeys(findTestObject('null'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Admin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Email@mailsac.com')

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////sysadmin new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))
//WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'SysAdmin-random')

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))
//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'SysAdmin')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))
//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-SysAdmin')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Email@mailsac.com')

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

//////////////////////////////////////////////////////////////////reviewer new

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

WebUI.click(details.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr/td[7]/div/a'))
//WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'Reviewer-random')

WebUI.click(role.getTestObjectWithXpath('//*[@id="select2-role-container"]'))
//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'Reviewer')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.click(ID.getTestObjectWithXpath('//*[@id="select2-id_type-container"]'))
//WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'),
	'KTP')

WebUI.sendKeys(findTestObject('null'),
	Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('null'), '1123')

WebUI.setText(findTestObject('null'), 'CEO-Reviewer')

WebUI.setText(findTestObject('null'), '123')

WebUI.setText(findTestObject('null'), 'Email@mailsac.com')

WebUI.setText(findTestObject('null'), 'OK')

WebUI.click(findTestObject('null'))

public class profile {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class logOut {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class btnLogout {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btnLogout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

////////////////////////////////////////////////

WebUI.click(findTestObject('null'))

WebUI.setText(findTestObject('null'), 'signer_osr')

WebUI.setEncryptedText(findTestObject('null'), '/5S6MFFLcE4mlsixtc6/Tg==')

WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))

public class companyM {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
WebUI.click(companyM.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[6]/a/span'))

/////////////////////////////////////////////////////////
 
public class detailApprove {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class companies {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class ApproveMA {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class noteMA {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
public class approveMA2 {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
}
 
WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////

WebUI.click(companies.getTestObjectWithXpath('//*[@id="0903"]/span'))

WebUI.setText(findTestObject('null'), 'Katalon-001')

WebUI.click(detailApprove.getTestObjectWithXpath('//*[@id="DataTables_Table_0"]/tbody/tr[1]/td[7]/a'))

WebUI.click(companies.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/div[3]/div/button[2]'))

WebUI.setText(noteMA.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[1]/div[2]/div/textarea'), 'ok')

WebUI.click(approveMA2.getTestObjectWithXpath('//*[@id="modal_approve"]/div/div/form/div[2]/button[2]'))

/////////////////////////////////////////////////////////////////////////////////////////

WebUI.click(profile.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(logOut.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.click(btnLogout.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))



