import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}


WebUI.openBrowser('')
WebUI.maximizeWindow()
WebUI.navigateToUrl(GlobalVariable.url)

////

WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userMakerAnchor)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))

WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a')) //Invoice AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0301"]')) //New Invoice A
WebUI.click(log.getTestObjectWithXpath('//*[@id="hours-available-progress"]/h3')) //Single Entry

WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-entitas_partner-container"]')) // select Company
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.anchor2)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.click(log.getTestObjectWithXpath('//*[@id="select2-payment_method-container"]'))
WebUI.setText(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), GlobalVariable.paymentSCFAP)
WebUI.sendKeys(log.getTestObjectWithXpath('/html/body/span[2]/span/span[1]/input'), Keys.chord(Keys.ENTER))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="amount"]'), GlobalVariable.amount)
public class benef {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

WebUI.setText(benef.getTestObjectWithXpath('//*[@id="numeric"]'), GlobalVariable.Rekening4B)

Date today = new Date()

String currentDate = today.format('dd-MM-yyyy')

String invDate = today.format('ddMMyyyy')

String invTime = today.format('hhmmss')

settle = (today + 1)

sharing = (today + 1)

String settlementDate = settle.format('dd-MM-yyyy')

String sharinglimitDate = settle.format('dd-MM-yyyy')

//String InvNo = (currentDate + '-') + currentTime
String noInv = (('StagCBM-AP' + invDate) + '-') + invTime

println(noInv)

println(currentDate)

println(settlementDate)

WebUI.setText(log.getTestObjectWithXpath('//*[@id="payment_date"]'), currentDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="maturity_date"]'), settlementDate)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="entitas_inv_no"]'), noInv)
WebUI.setText(log.getTestObjectWithXpath('/html/body/div[2]/div[2]/div[2]/div/div/div/form/div[1]/fieldset/div[11]/div/input'), noInv)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submit-user"]/button'))
WebUI.delay(20)
WebUI.verifyTextPresent('successful', true, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a/span'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

//// checker

WebUI.setText(log.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userCheckerAnchor)
//password
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

//button sumbit
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
/////////////////
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
//inbox invoice AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0304"]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_checker_confirm"]/div/div/form/div[2]/button[2]'))
//Verifikasi sukses
WebUI.verifyTextPresent('success', false, FailureHandling.STOP_ON_FAILURE)
//Log out
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))

/// signer

//username
WebUI.setText(log.getTestObjectWithXpath('/html/body/div/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userSignerAnchor)
//password
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)

//button sumbit
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
/////////////////
WebUI.click(log.getTestObjectWithXpath('/html/body/div[2]/div[1]/div[2]/div[2]/ul/li[4]/a'))
//inbox invoice AP
WebUI.click(log.getTestObjectWithXpath('//*[@id="0305"]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="row_1"]/td[9]/div/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="approve"]'))
WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'OK')
WebUI.setText(log.getTestObjectWithXpath('//*[@id="otp_code"]'), GlobalVariable.otpCode)
 
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_approve_signer_confirm"]/div/div/form/div[2]/button[2]'))

WebUI.delay(5)

WebUI.takeScreenshot()

if(WebUI.verifyTextPresent('successful', true, FailureHandling.CONTINUE_ON_FAILURE)){
	
	WebUI.delay(1)

//Log out
	WebUI.takeScreenshot()
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	println("---------success-------------------")
	
	}else {
	WebUI.takeScreenshot()
	WebUI.click(log.getTestObjectWithXpath('//*[@id="reject"]'))
	WebUI.setText(log.getTestObjectWithXpath('//*[@id="modal_reject_signer_confirm"]/div/div/form/div[1]/div/div/textarea'), 'gagal approve')
	WebUI.click(log.getTestObjectWithXpath('//*[@id="reject_signer"]'))
	println("---------invoice rejected")
	WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[4]'))
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
	}

//close

WebUI.closeBrowser()

