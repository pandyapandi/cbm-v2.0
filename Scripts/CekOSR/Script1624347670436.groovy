import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

public class login {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}

public class log {
	public static TestObject getTestObjectWithXpath(String xpath) {
		return new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
	}
	
}
WebUI.delay(100)
WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.url)
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_information"]/div/div/div[3]/button'), FailureHandling.OPTIONAL)
WebUI.delay(1)
WebUI.setText(log.getTestObjectWithXpath('/html/body/div[1]/div[1]/div/div/div[2]/form/div[1]/input'), GlobalVariable.userOSR)
WebUI.setText(log.getTestObjectWithXpath('//*[@id="identk"]'), GlobalVariable.password)
WebUI.delay(1)
WebUI.callTestCase(findTestCase('Chaptcha'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.click(log.getTestObjectWithXpath('//*[@id="submitsignin"]'))
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="disbursemet_completed_open"]'))
WebUI.acceptAlert(FailureHandling.OPTIONAL)
WebUI.delay(5)
WebUI.takeScreenshot()
WebUI.delay(5)

WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/a'))
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="navbar-mobile"]/ul[2]/li/div/a[3]'))
WebUI.delay(1)
WebUI.click(log.getTestObjectWithXpath('//*[@id="modal_logout"]/div/div/div[3]/a'))
WebUI.delay(1)
WebUI.closeBrowser()

