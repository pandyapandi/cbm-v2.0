<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>STAGING</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e37e1f2f-6098-4a0a-ae0d-a38161743599</testSuiteGuid>
   <testCaseLink>
      <guid>f6dba9a3-72e6-4d94-93f7-9873c0fb641c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AP Anchor-Anchor/SCF AP Anchor-Anchor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a60d4e3c-35a0-46f3-b994-1fa2dd3fc23e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AP Anchor-Partner Without/SCF AP Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2d76afc-94a6-4e10-ad7d-1c845543b087</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AP Partner-Anchor With/SCF AP Partner-Anchor With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31292f5f-7bc0-4f16-a97b-e5aec2a08f44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/ARK/AR Kombinasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65c17fbb-1463-4648-b8a7-ba5d7686ea30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/DF Anchor-Partner With/DF Anchor-Partner With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31157672-e248-4fea-8371-4763cf2871d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/DF Anchor-Partner Without/DF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8160f51b-ee5f-4329-be91-c3c0ec88c187</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/Instapay DF Without/DF Instapay Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2da3f12b-2050-4053-bece-d80d8dc64b34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AR Anchor-Anchor Without/SCF AR Anchor-Anchor Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c551d71c-1892-4199-aeff-d07936f4a1ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AR Anchor-Partner With/SCF AR Anchor-Partner With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c7d1143-aa30-4aed-a2b1-56abe1bb69f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/SCF AR Anchor-Partner Without/SCF AR Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db8f18d3-32e3-484b-81f3-e62d918770fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/VF Anchor-Partner Without/VF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4c00c3f-8bb6-4a4b-9097-f8dcc6006f16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/STAGING/Complete Invoice/Invoice Successful/VF Partner-Anchor With/VF Partner-Anchor With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c265fd00-a3a9-4e47-bebd-5f60827fb458</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CekOSR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
