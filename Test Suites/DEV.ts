<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DEV</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3ac56ebd-fcbe-4da9-8335-b5caf7d50342</testSuiteGuid>
   <testCaseLink>
      <guid>2621720b-818b-48b2-9857-2889163a2018</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/ARK/AR Kombinasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23b26630-abfc-4372-a6a3-a2f3f7e91c35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/DF Anchor-Partner With/DF Anchor-Partner With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46791a54-148c-466b-a941-5022631f2ffc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/DF Anchor-Partner Without/DF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9041494a-e3c1-46c7-98eb-f3bd989c6c3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AP Anchor-Anchor/SCF AP Anchor-Anchor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39cfb8be-9c9d-4fc8-b60f-1f8e5900f40c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AP Anchor-Partner Without/SCF AP Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cf736a5-e70c-45b2-9eef-e5f621ee8b1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AP Partner-Anchor With/SCF AP Partner-Anchor With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb087d36-262b-41e0-938d-82c520b25431</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AR Anchor-Anchor Without/SCF AR Anchor-Anchor Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>624eb94f-962d-4d93-a259-006702c124cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AR Anchor-Partner With/SCF AR Anchor-Partner With</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9eeba3d-05c9-4219-ab25-3eb72710a2fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/SCF AR Anchor-Partner Without/SCF AR Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78a6e826-a66b-42a4-88ef-897f7e89f731</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/VF Anchor-Partner Without/VF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b39a563-4569-4736-95d0-c490e4fb1b98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DEV/Complete Invoice/Invoice Successful/VF Partner-Anchor With/VF Partner-Anchor With</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
