<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Prod</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ba95e72e-2ba0-4f1b-94fb-5e3ef8a7fb94</testSuiteGuid>
   <testCaseLink>
      <guid>b0b1fc7c-8cde-4af8-b729-5a4d5e523693</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/SCF AR Anchor-Partner Without/SCF AR Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9550934-8358-44aa-9f78-6a8cefa6b9cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/SCF AP Anchor-Partner Without/SCF AP Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>148542a6-8428-48d4-8d90-c0d2937f72a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/ARK/AR Kombinasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a703cd14-748b-4d69-ab2c-6b622eeee3a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/DF Anchor-Partner Without/DF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7b96533-3be1-4743-a121-4428070e3abe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/VF Anchor-Partner Without/VF Anchor-Partner Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2557fb89-bc8f-4da4-974c-d4464d28c5f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Bulk Upload AR/BulkUpload AR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f64c38da-339b-4e97-80a2-b349a433ae47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Bulk Upload AP/BulkUpload AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0702f69e-f942-47ef-a302-ddaf42134688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/AR Partner/AR Partnership</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec10b3dd-856a-408c-84a0-ca4e047a77e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Instapay DF Without/DF Instapay Without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>566a69a1-542f-42ca-a531-69626c9384ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Cancel/Cancel AP/Cancel SCF AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>423da9db-85a4-45ca-9895-ac6725e74d37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Cancel/Cancel AR/Cancel SCF AR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4ea3c62-53b3-427b-8da5-ade92145f3f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Cancel/Cancel ARP/Cancel AR Partnership</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6956dfcc-ae94-471e-8643-84d8b6b95e08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Cancel/Cancel DF/Cancel DF without</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>362644ce-b166-4e14-9140-4daec103a2d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PROD/Complete Invoice/Invoice Successful/Cancel/Cancel VF/Cancel VF without</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
